package main

// const: STATUS 1 ACTIVE, 2 SUSPENDED, 2 REVOKED
const (
	UPDATEBASE    = 0
	SUSPENDSTATUS = 2
	ACTIVESTATUS  = 1
	REVOKESTATUS  = 3
)

// const: Entity of project for Key Manage
const (
	AIMSTATUS = "AIMSTATUS"
	AIMBASE   = "AIMBASE"
	AIM       = "AIM"
)

// const: ROLE e UID attribute
const (
	ROLE = "role"
	UUID = "uuid"
	ADDR = "addr"
)

// const: ECDSA e RSA algotithm for Public Key
const (
	ECDSA = "ECDSA"
	RSA   = "RSA"
)

// const: ROLE 0 USER, ROLE 1 ADMIN, ROLE 2 CREATOR
const (
	USER    = "user"
	ADMIN   = "admin"
	CREATOR = "creator"
)

// const: Max N umber of Generations SID - Controller
const (
	NUMMAXGEN = 5
)

// var: CHARACTER ALLOWED and Limit max for string and for url
var (
	ERROR   = Activity{"ERROR!!", 3}
	WARNING = Activity{"WARNING", 2}
	INFO    = Activity{"INFO***", 1}
	DEBUG   = Activity{"DEBUG->", 0}
)

const (
	CHALLENGERESPONSE = "TenIdChallengeResponse"
)


const (
	POSTIDENTITY          = "postIdentity"
	POSTIDENTITYROOT      = "postIdentityRoot"
	SUSPENDIDENTITY       = "suspendIdentity"
	ACTIVATEIDENTITY      = "activateIdentity"
	REVOKEIDENTITY        = "revokeIdentity"
	EDITIDENTITY          = "editIdentity"
	GETIDENTITY           = "getIdentity"
	GETALLIDENTITIES      = "getAllIdentities"
	GETIDENTITIESFILTERED = "getIdentitiesFiltered"
	GETHIERARCHYIDENTITY  = "getHierarchyIdentity"
)