package main

// IdentityBase : First part of Identity - Account Identity Manifest Base
type IdentityBase struct {
	Id    string `json:"id"`
	PKey  string `json:"pKey"`
	KType string `json:"kType"`
	End   string `json:"end"`
	Ext   string `json:"ext"`
}
