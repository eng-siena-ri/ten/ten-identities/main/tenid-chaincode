package main

// IdentityStatus : IdentityStatus second part of Identity - Account Identity Manifest Status
type IdentityStatus struct {
	Id     string `json:"id"`
	Start  string `json:"start"`
	Status int    `json:"status"` // 1: ACTIVE, 2: SUSPENDED, 3: REVOKED
}
