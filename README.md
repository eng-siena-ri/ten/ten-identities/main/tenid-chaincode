# tenid-chaincode


The system requires the go version 1.14.

Clone the chaincode tenid-chaincode from the repository into gopath /src


launch the following commands in sequence:

- go mod init

- go mod tidy

- go mod vendor


To deploy tenid-chaincode smartcontract (Hyperledger Fabric 2.2) exists a script sh:

**network.sh**

This script needs some parameters:

deployCC, name, version, path, init.
The Init function should be called in order to initialize the domain data. 



Example:

`./network.sh deployCC -ccp ./chaincode/go/tenid-chaincode -ccn tenIdentitiesInterQ -ccv 1 -cci init -ccs 1 -ccl Go`
 

Used with network.sh deployCC
- -c <channel name> - Name of channel to deploy chaincode to
- -ccn <name> - Chaincode name. This flag can be used to deploy one of the asset transfer samples to a channel. Sample options: basic (default),ledger, private, sbe, secured
- -ccl <language> - Programming language of the chaincode to deploy: go (default), java, javascript, typescript
- -ccv <version>  - Chaincode version. 1.0 (default), v2, version3.x, etc
- -ccs <sequence>  - Chaincode definition sequence. Must be an integer, 1 (default), 2, 3, etc
- -ccp <path>  - (Optional) File path to the chaincode. When provided, the -ccn flag will be used only for the chaincode name.

The documentation is here:

[https://hyperledger-fabric.readthedocs.io/en/release-2.2/deploy_chaincode.html](https://hyperledger-fabric.readthedocs.io/en/release-2.2/deploy_chaincode.html)

The chaincode will always return with an object of type Response:

`type Response struct {
Header,  
Message string
}`


In the message string there will be the value of the chaincode response in case of valid operation
The header is a structure:

`type Header struct {
Code        int,
Description string
}`


In the Header object we find two fields, an entire code that identifies the status code of the operation performed (as for the http protocol, es. 201, 404....) and a description field where it will be possible to find a small description of the code just received.

**License**

identities-command-line Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the LICENSE file.
