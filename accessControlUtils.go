package main

import (
	"fmt"
	"strings"

	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-chaincode-go/shim"
)

func getTxCreatorInfo(stub shim.ChaincodeStubInterface) (string, string, error) {

	//var mspid string
	var err error
	var uuid, role string
	var found bool

	role, found, err = cid. GetAttributeValue(stub, ROLE)
	if err != nil {
		fmt.Printf("Error getting Attribute Value: %s\n", err.Error())
		return "", "", err
	}
	if found == false {
		fmt.Printf("Error getting ROLE --> NOT FOUND!!!\n")
		return "", "", nil
	}
	fmt.Printf("ROLE FOUND: %s\n", role)

	uuid, found, err = cid. GetAttributeValue(stub, UUID)
	if err != nil {
		fmt.Printf("Error getting Attribute Value hf.EnrollmentID: %s\n", err.Error())
		return "", "", err
	}
	if found == false {
		fmt.Printf("Error getting UUID --> NOT FOUND!!!\n")
		return "", "", nil
	}
	fmt.Printf("UUID FOUND: %s\n", uuid)

	//return attrValue1, attrValue2, nil
	return role, uuid, nil
}

func isInvokerOperator(stub shim.ChaincodeStubInterface, attrName string) (bool, string, error) {
	var found bool
	var attrValue string
	var err error

	attrValue, found, err = cid.GetAttributeValue(stub, attrName)
	if err != nil {
		fmt.Printf("Error getting Attribute Value: %s\n", err.Error())
		return false, "", err
	}
	return found, attrValue, nil
}

func getIdCa(stub shim.ChaincodeStubInterface) (string, error) {
	var idCa string
	var err error

	idCa, err = cid. GetID(stub)
	if err != nil {
		fmt.Printf("Error getting id CA Fabric: %s\n", err.Error())
		return  "", err
	}

	return idCa, nil
}

func getIdSubjectCert(stub shim.ChaincodeStubInterface) (string, error) {
	var s string = ""
	var mspid string = ""
	var commonName string = ""
	cert, err := cid.GetX509Certificate(stub)
	if err != nil {
		fmt.Printf("Error getting id CA Fabric: %s\n", err.Error())
		return "", err
	}

	mspid, err = cid.GetMSPID(stub)
	if err != nil {
		fmt.Printf("Error getting id CA Fabric: %s\n", err.Error())
		return  "", err
	}

	commonName = cert.Subject.CommonName
	s = strings.ToLower(mspid + commonName)
	return s, nil
}

// ThisIsTheRole return a boolean for the role searched
func ThisIsTheRole(stub shim.ChaincodeStubInterface, roleControl string) (bool, bool, error) {
	var ok bool = false
	var found bool
	var role string
	var err error

	// Who is the owner? Is it authorized to create a new ObjectDistribution (CREATOR)?
	found, role, err = isInvokerOperator(stub, ROLE)
	if err != nil {
		Log(ERROR, "SmartIndustryChaincode ThisIsTheRole isInvokerOperator err not nil", err.Error())
		return false, false, err
	}

	if role == roleControl {
		ok = true
	} else {
		Log(WARNING, "SmartIndustryChaincode postObjectDistribution ERROR: role is not creator (super-user)!!!", role)
	}
	return ok, found, nil

}
