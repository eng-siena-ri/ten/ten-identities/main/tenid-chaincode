package Models

// PIP : Data of Identity Provider (PIdS.Payload - if PIdS.Role == "PIP")
type PIP struct {
	SidAuth string `json:"sidAuth"`
	SidCert string `json:"sidCert"`
	PpaSubs string `json:"ppaSubs"`
	PpaEmit string `json:"ppaEmit"`
}
