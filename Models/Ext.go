package Models

// Ext : Ext Generic
type Ext struct {
	ExtType string `json:"extType"` // PIdS or RWE
	Payload string `json:"payload"`
}
