package Models

// RWEPayload : Real World Entities Generic
type RWE struct {
	Nature  string `json:"nature"` // N or L
	Payload string `json:"payload"`
}

// LegalPerson : Legal Person (RWE.Payload - if RWE.Nature == "L")
type LegalPerson struct {
	FiscalCode   string `json:"fiscalCode"`
	BusinessName string `json:"businessName"`
	Address      string `json:"address"`
}
