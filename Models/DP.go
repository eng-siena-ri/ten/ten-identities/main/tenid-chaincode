package Models

// DP : Data Provider  (PIdS.Payload - if PIdS.Role == "DP")
type DP struct {
	SidAuthNot string `json:"sidAuthNot"`
	PpaSubsNot string `json:"ppaSubsNot"`
	NetEvent   string `json:"netEvent"`
}
