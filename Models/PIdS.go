package Models

// PIdS : ProtectId Services Generic
type PIdS struct {
	Role    string `json:"role"` // PIP or DP
	Payload string `json:"payload"`
}
