package main

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"github.com/PaesslerAG/jsonpath"

	"sort"
	"strconv"
	"strings"

	"github.com/btcsuite/btcutil/base58"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"time"

	pb "github.com/hyperledger/fabric-protos-go/peer"
)

var caAccess bool

//tenIdentities struct
type tenIdentities struct {
}

//Init method
func (t *tenIdentities) Init(stub shim.ChaincodeStubInterface) pb.Response {
	Log(INFO, "---INIT TEN-IDENTITY SMART CONTRACT---", "")
	return shim.Success(nil)
}

// Invoke is called per transaction on the chaincode. Each transaction is
// either a 'get' or a 'set' on the asset created by Init function. The Set
// method may create a new asset by specifying a new key-value pair.
func (t *tenIdentities) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	// Variable

	Log(INFO, "tenIdentities function invoked: ", fn)
	if fn == "postIdentity" {
		return t.postIdentity(stub, args)
	} else if fn == "postIdentityRoot" {
		return t.postIdentity(stub, args)
	} else if fn == "suspendIdentity" {
		return t.editIdentity(stub, SUSPENDSTATUS, args)
	} else if fn == "activateIdentity" {
		return t.editIdentity(stub, ACTIVESTATUS, args)
	} else if fn == "revokeIdentity" {
		return t.editIdentity(stub, REVOKESTATUS, args)
/*	15/02/2022: Now is not possible update the ext field.
	} else if fn == "editIdentity" {
		return t.editIdentity(stub, UPDATEBASE, args)
 */
	} else if fn == "getIdentity" {
		return t.getIdentity(stub, args)
	} else if fn == "getAllIdentities" {
		return t.getAllIdentities(stub, args)
	} else if fn == "getIdentitiesFiltered" {
		return t.getIdentitiesFiltered(stub, args)
	} else if fn == "verifySignature" {
		return t.verifySignature(stub, args)
	} else if fn == "getHierarchyIdentity" {
		return t.getHierarchyIdentity(stub, args)
	} else {
		return shim.Error(ValResp(400, "Invalid function name in Invoke"))
	}
}

//*************************************************************
// MANAGE IDENTITY
//*************************************************************

//postIdentity: the input json must contain the object Identity,
//The caller must be a MEMBER/ADMIN!!!

func (t *tenIdentities) postIdentity(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	Log(INFO, "tenIdentities ON postIdentity", time.Now().Format(time.RFC3339))

	var jsonResp string
	var err error
	var aim Identity = Identity{}
	var epkxy ECDSApublicKeyXY = ECDSApublicKeyXY{}
	var rpken RSApublicKeyEN = RSApublicKeyEN{}
	var byteEpkxy []byte
	var byteRpken []byte
	var aimEnd Identity = Identity{}
	var desc string
	var idCa = ""
	var datetime time.Time
	var byteAim []byte
	var aimBaseOUT IdentityBase = IdentityBase{}
	var byteAimBase []byte
	var aimStatus IdentityStatus = IdentityStatus{}
	var byteAimStatus []byte
	var IDKey string
	var found bool
	var role string

	// args[0] = aim, args[1] = signature
	if len(args) != 2 {
		Log(ERROR, "tenIdentities postIdentity incorrect numbers of Args: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(422, "postIdentity error: incorrect numbers of Args"+strconv.Itoa(len(args))))
	}

	// Create Object from args parameters
	err = json.Unmarshal([]byte(args[0]), &aim)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity jsonUnmarshal Identity err not nil", err.Error())
		return shim.Error(ValResp(422, err.Error()))
	}
	Log(INFO, "tenIdentities postIdentity for this object Identity:", args[0])

	datetime = time.Now()

	// Control Aim into CA and check the authorization to postIdentity
	// GetAttributeValue(ROLE) - notfound is KO
	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(422, err.Error()))
	}

	/*found, role, err = isInvokerOperator(stub, ROLE)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity isInvokerOperator(role) err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if !found {
		Log(ERROR, "tenIdentities postIdentity ERROR: ROLE is empty!!!", "")
		return shim.Error(ValResp(404, "tenIdentities postIdentity ERROR: ROLE is empty!!!"))
	}
	Log(DEBUG, "tenIdentities postIdentity isInvokerOperator by role: ", role)
	*/

	/* if role != CREATOR && role != ADMIN {
		Log(ERROR, "tenIdentities postIdentity ERROR: ROLE is not authorized to Post Aim!!", role)
		return shim.Error("tenIdentities postIdentity ERROR: ROLE is not authorized to Post Aim!!")
	} */

	// aimBaseOut is aim in output
	// aim.AimBase is aim in input
	aimBaseOUT = aim.IdentityBase

	// We are going to create a new aim that isn't the ROOT of the system. ROOT: End = Null.
	// NO:
	if aim.IdentityBase.End != "" {

		aimEnd, desc, err = SearchIdentity(stub, aimBaseOUT.End, datetime)
		if err != nil {
			Log(ERROR, "tenIdentities postIdentity Addr Endorser err not nil", err.Error())
			return shim.Error(ValResp(422, err.Error()))
		}
		if (aimEnd == Identity{}) {
			Log(ERROR, "tenIdentities postIdentity Addr Endorser NOTFOUND ", desc)
			return shim.Error(ValResp(422, "tenIdentities postIdentity Addr Caller NOTFOUND "+desc))
		}

		// The chain of trust is ok. The End is Active and all Endorsers connected to him are actives

		// AIM BASE  ///////////////////////////////////////////////////////////////////////////////////////////

		// Control the signature with random text found
		text := POSTIDENTITY + args[0] + idCa
		validate, desc, err := ValidateChallengeQueue(text, aimEnd.PKey, aimEnd.KType, args[1])
		if err != nil {
			Log(ERROR, "tenIdentities postIdentity ValidateChallengeQueue err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		if !validate {
			Log(ERROR, desc, "")
			return shim.Error(ValResponse(401, desc, CHALLENGERESPONSE))
		} else {
			Log(INFO, "tenIdentities postIdentity validate signature ", desc)
		}

		aimBaseOUT.Id = createAddressFromPKeyBlob(aim.IdentityBase.PKey)
		Log(DEBUG, "tenIdentities postIdentity Address generated: ", aimBaseOUT.Id)

	} else {
		// We are going to create the ROOT of the system (Endorser = Id address) >>>> ONLY FABRIC ADMIN CAN DO THIS (TO CREATE ROOT)
		// This control is only for the creation of ROOT: getAttribute Role is only here (now).
		// if caAccess && role != ADMIN {
		if caAccess {
			found, role, err = isInvokerOperator(stub, ROLE)
			if err != nil {
				Log(ERROR, "tenIdentities postIdentityRoot isInvokerOperator(role) err not nil", err.Error())
				return shim.Error(ValResp(400, err.Error()))
			}
			if !found {
				Log(ERROR, "tenIdentities postIdentityRoot ERROR: attribute ROLE is empty!!!", "NOT FOUND")
				return shim.Error(ValResp(404, "tenIdentities postIdentityRoot ERROR: ROLE is empty!!!"))
			}
			Log(DEBUG, "tenIdentities postIdentityRoot isInvokerOperator by role: ", role)
			if role != ADMIN {
				Log(ERROR, "postIdentityRoot error: invalid Identity (caller cannot create a chain of trust root)", role)
				return shim.Error(ValResp(401, "postIdentity error: invalid Identity (caller cannot create a chain of trust root)!!"))
			}
		}
		if aim.IdentityBase.KType == ECDSA {
			err = json.Unmarshal([]byte(aim.IdentityBase.PKey), &epkxy)
			if err != nil {
				Log(ERROR, "tenIdentities postIdentityRoot jsonUnmarshal signature err not nil", err.Error())
				return shim.Error(ValResp(422, err.Error()))
			}

			byteEpkxy, err = json.Marshal(epkxy)
			if err != nil {
				Log(ERROR, "tenIdentities postIdentityRoot jsonMarshal epkxy err not nil", err.Error())
				return shim.Error(ValResp(422, err.Error()))
			}
			aimBaseOUT.PKey = string(byteEpkxy)
		} else if aim.IdentityBase.KType == RSA {

			err = json.Unmarshal([]byte(aim.IdentityBase.PKey), &rpken)
			if err != nil {
				Log(ERROR, "tenIdentities postIdentityRoot jsonUnmarshal PKyBlob in RSA Public Key err not nil", err.Error())
				return shim.Error(ValResp(422, err.Error()))
			}

			byteRpken, err = json.Marshal(rpken)
			if err != nil {
				Log(ERROR, "tenIdentities postIdentityRoot jsonMarshal rpken err not nil", err.Error())
				return shim.Error(ValResp(422, err.Error()))
			}
			aimBaseOUT.PKey = string(byteRpken)
		} else {
			Log(ERROR, "tenIdentities postIdentityRoot ERROR: algorithm for the Public Key is not admitted!! ", aim.IdentityBase.KType)
			return shim.Error(ValResp(422, "postIdentityRoot error: invalid Identity (invalid key type)"))
		}
		// Create address
		aimBaseOUT.Id = createAddressFromPKeyBlob(aim.IdentityBase.PKey)
		Log(DEBUG, "tenIdentities postIdentityRoot Address generated: ", aimBaseOUT.Id)
		// We are going to create a ROOT of System -> Root END == ID.
		aimBaseOUT.End = aimBaseOUT.Id
		Log(DEBUG, "tenIdentities postIdentityRoot Address generated for the ROOT: ", aimBaseOUT.Id)

		// Control the signature with random text found
		text := POSTIDENTITYROOT + args[0] + idCa
		validate, desc, err := ValidateChallengeQueue(text, aimBaseOUT.PKey, aimBaseOUT.KType, args[1])
		if err != nil {
			Log(ERROR, "tenIdentities postIdentityRoot ROOT ValidateChallengeQueue err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		if !validate {
			Log(ERROR, desc, "")
			return shim.Error(ValResponse(401, desc, CHALLENGERESPONSE))
		} else {
			Log(INFO, "tenIdentities postIdentityRoot ROOT validate signature ", desc)
		}
	}
	// Control if the record AimBase is there into the Ledger
	// GetIdentityBase with this Address
	// Create the KEY

	IDKey, err = stub.CreateCompositeKey(AIMBASE, []string{aimBaseOUT.Id})
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity CreateCompositeKey err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	Log(DEBUG, "tenIdentities postIdentity for the key:", IDKey)
	byteAimBase, err = stub.GetState(IDKey)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity GetState err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if byteAimBase != nil {
		Log(ERROR, "postIdentity error: invalid Identity (address is not unique)", aimBaseOUT.Id)
		return shim.Error(ValResp(409, "postIdentity error: invalid Identity (address is not unique)"))
	}

	// Convert Object AimBase in []Byte
	byteAimBase, err = json.Marshal(&aimBaseOUT)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity jsonMarshal IdentityBase err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	// PUT STATE INTO THE LEDGER
	err = stub.PutState(IDKey, byteAimBase)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity putState(IdentityBase) err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	Log(INFO, "tenIdentities postIdentity putState(IdentityBase) is OK with address: ", IDKey)

	// STATUS ENTRY ////////////////////////////////
	aimStatus = aim.IdentityStatus
	aimStatus.Id = aimBaseOUT.Id
	aimStatus.Start = datetime.Format(time.RFC3339)

	if aim.IdentityStatus.Status != SUSPENDSTATUS &&
		aim.IdentityStatus.Status != ACTIVESTATUS {
		Log(INFO, "postIdentity error: invalid Identity (invalid status)", string(aim.IdentityStatus.Status))
		return shim.Error(ValResp(422, "postIdentity error: invalid Identity (invalid status)"+string(aim.IdentityStatus.Status)))
	}

	// Create the KEY
	IDKey, err = stub.CreateCompositeKey(AIMSTATUS, []string{aimStatus.Id, aimStatus.Start})

	// Convert Object AimStatus in []Byte
	byteAimStatus, err = json.Marshal(&aimStatus)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity jsonMarshal IdentityStatus err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	// PUT STATE INTO THE LEDGER
	err = stub.PutState(IDKey, byteAimStatus)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity putState(IdentityStatus) err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	Log(INFO, "tenIdentities postIdentity putState(IdentityStatus) is OK with address: ", IDKey)

	aim.IdentityBase = aimBaseOUT
	aim.IdentityStatus = aimStatus

	byteAim, err = json.Marshal(&aim)
	if err != nil {
		Log(ERROR, "tenIdentities postIdentity jsonMarshal Identity err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAim)
	Log(INFO, "tenIdentities postIdentity putState is OK with response:", jsonResp)

	return shim.Success([]byte(ValResponse(201, "tenIdentities postIdentity putState is OK", string(byteAim))))

}

//editIdentity: the input json must contain the object Aim to EDIT,
//The caller must be a MEMBER/ADMIN!!!

func (t *tenIdentities) editIdentity(stub shim.ChaincodeStubInterface, funcEdit int, args []string) pb.Response {
	// SUSPENDSTATUS = 2 - ACTIVESTATUS  = 1 - REVOKESTATUS  = 3

	Log(INFO, "tenIdentities ON editIdentity with function: ", strconv.Itoa(funcEdit))

	var jsonResp string
	var IDKey string
	var err error
	var aim Identity = Identity{}
	var byteAim []byte
	var aimBase IdentityBase = IdentityBase{}
	var byteAimBase []byte
	var aimStatus IdentityStatus = IdentityStatus{}
	var byteAimStatus []byte
	var aimStatusEntries []IdentityStatus
	// var found bool
	var idIn string
	var signature string
	var endorserIn string
	var extIn string
	var aimEndorser Identity = Identity{}
	var desc string
	var datetime time.Time
	var functionName string
	var idCa = ""

	// Control args[]: identity, signature
	if len(args) != 2 {
		Log(ERROR, "tenIdentities editIdentity numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities editIdentity numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	// Create Object from args parameters
	err = json.Unmarshal([]byte(args[0]), &aim)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity jsonUnmarshal Identity err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	Log(INFO, "tenIdentities editIdentity for this object Identity:", args[0])

	idIn = aim.IdentityBase.Id
	endorserIn = aim.IdentityBase.End
	extIn = aim.IdentityBase.Ext

	signature = args[1]

	datetime = time.Now()

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	// Control Aim into CA and check the authorization to editIdentity
	// GetAttributeValue(ROLE) - notfound is KO
	/*
		found, role, err = isInvokerOperator(stub, ROLE)
		if err != nil {
			Log(ERROR, "editIdentity error: isInvokerOperator(role) err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		if !found {
			Log(ERROR, "editIdentity error: ROLE is empty!!!", "")
			return shim.Error(ValResp(404, "editIdentity ERROR: ROLE is empty"))
		}
		Log(DEBUG, "editIdentity isInvokerOperator by role: ", role)
		if role != ADMIN {
			Log(ERROR, "editIdentity error: invalid Identity (caller cannot create a chain of trust root)", role)
			return shim.Error(ValResp(401, "editIdentity error: invalid Identity (caller cannot create a chain of trust root)!"))
		}
	*/

	// Create ID KEY
	IDKey, err = stub.CreateCompositeKey(AIMBASE, []string{idIn})
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity CreateCompositeKey err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	Log(DEBUG, "tenIdentities editIdentity for the key:", IDKey)
	byteAimBase, err = stub.GetState(IDKey)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity GetState err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if byteAimBase == nil {
		Log(ERROR, "editIdentity error: Identity not found", IDKey)
		return shim.Error(ValResp(404, "tenIdentities editIdentity IdentityBase DATA NOT FOUND func EDIT not possible"))

	}
	err = json.Unmarshal(byteAimBase, &aimBase)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity Unmarshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	// Only your Endorser (parameter args[2]) can modify the status of a aim
	if aimBase.End != endorserIn {
		Log(ERROR, "editIdentity error: Endorser mismatch", endorserIn+" "+aimBase.End)
		return shim.Error(ValResp(401, "editIdentity error: endorser mismatch"+endorserIn+" "+aimBase.End))
	}

	aimEndorser, desc, err = SearchIdentity(stub, aimBase.End, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity Addr Endorser err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aimEndorser == Identity{}) {
		Log(ERROR, "tenIdentities editIdentity Addr Endorser NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities EditIdentity Addr Endorser NOTFOUND "+desc))
	}

	// The chain of trust is ok. The Endorser is Active and all Endorsers connected to him are actives

	// BASE ENTRY //////////////////////////////////////////////////////////////////////////////////////////

	if funcEdit == REVOKESTATUS {
		functionName = REVOKEIDENTITY
	} else if funcEdit == SUSPENDSTATUS {
		functionName = SUSPENDIDENTITY
	} else if funcEdit == ACTIVESTATUS {
		functionName = ACTIVATEIDENTITY
	} else if funcEdit == UPDATEBASE {
		functionName = EDITIDENTITY
	}

	text := functionName + args[0] + idCa
	validate, desc, err := ValidateChallengeQueue(text, aimEndorser.PKey, aimEndorser.KType, signature)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(401, err.Error()))
	}
	if !validate {
		Log(ERROR, desc, "")
		return shim.Error(ValResp(400, desc))
	} else {
		Log(INFO, "tenIdentities editIdentity validate signature ", desc)
	}

	// Search aimStatus for this aimBase
	aimStatusEntries, err = GetStateByPartialCompositeKeyStatusEntry(stub, []string{aimBase.Id})
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity GetStateByPartialCompositeKey IdentityStatus err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if len(aimStatusEntries) == 0 || aimStatusEntries == nil {
		Log(ERROR, "tenIdentities editIdentity GetStateByPartialCompositeKey IdentityStatus DATA NOT FOUND", IDKey)
		return shim.Error(ValResp(404, "editIdentity error: invalid Identity (status not found)"))
	}

	aimStatus = aimStatusEntries[len(aimStatusEntries)-1]

	// Control Status into the Ledger
	if aimStatus.Status == REVOKESTATUS {
		if funcEdit != REVOKESTATUS {
			Log(ERROR, "editIdentity error: invalid Identity (invalid status)", aimBase.Id)
			return shim.Error(ValResp(400, "editIdentity error: invalid Identity (invalid status)"))
		}
	}

	//  UPDATE the field EXT of IDENTITY BASE
	if funcEdit == UPDATEBASE {

		if aimStatus.Status == REVOKESTATUS || aimStatus.Status == SUSPENDSTATUS {
			Log(ERROR, "tenIdentities editIdentity: Identity is NOT ACTIVE:", aimBase.Id)
			return shim.Error(ValResp(409, "editIdentity error: conflict with the current status (REVOKED or SUSPENDED) of the resource."))
		}

		// Control EXT not Blank (???)
		//if extIn == "" {
		//	Log(ERROR, "tenIdentities editIdentity EXT in INPUT is not valorized:",extIn )
		//	return shim.Error(ValResp(400, "tenIdentities editIdentity UPDATE EXT but in INPUT is not valorized!"))
		//}
		aimBase.Ext = extIn
		Log(DEBUG, "tenIdentities editIdentity: Identity NOW is UPDATED the address: ", aimBase.Id)
		Log(DEBUG, "tenIdentities editIdentity: Identity NOW is UPDATED with the new EXT ", aimBase.Ext)

		// Create ID KEY
		IDKey, err = stub.CreateCompositeKey(AIMBASE, []string{aimBase.Id})
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity CreateCompositeKey IdentityBase err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		// Convert Object AimBase in []Byte
		byteAimBase, err = json.Marshal(&aimBase)
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity jsonMarshal IdentityBase err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		// PUT STATE INTO THE LEDGER
		err = stub.PutState(IDKey, byteAimBase)
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity putState(IdentityBase) err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		Log(INFO, "tenIdentities editIdentity putState(IdentityBase) is OK with address: ", IDKey)

	} else {

		//  UPDATE the STATUS of IDENTITY (new record AIM STATUS with the same address and date now).
		if extIn != "" {
			Log(ERROR, "tenIdentities editIdentity EXT in INPUT is not null:", extIn)
			return shim.Error(ValResp(400, "tenIdentities editIdentity UPDATE STATUS but in INPUT is not null!"))
		}

		aimStatus.Start = datetime.Format(time.RFC3339)

		// Set the value of new status
		if funcEdit == ACTIVESTATUS {
			if aimStatus.Status == ACTIVESTATUS {
				Log(WARNING, "tenIdentities editIdentity: Identity is already ACTIVE", aimBase.Id)
				return shim.Error(ValResp(409, "editIdentity error: conflict with the current status (ACTIVE) of the resource."))
			}
			Log(DEBUG, "tenIdentities editIdentity: Identity NOW is ACTIVE ", aimBase.Id)
			aimStatus.Status = ACTIVESTATUS
		} else if funcEdit == SUSPENDSTATUS {
			if aimStatus.Status == SUSPENDSTATUS {
				Log(WARNING, "tenIdentities editIdentity: Identity is already SUSPENDED", aimBase.Id)
				return shim.Error(ValResp(409, "editIdentity error: conflict with the current status (SUSPENDED) of the resource."))
			}
			aimStatus.Status = SUSPENDSTATUS
			Log(DEBUG, "tenIdentities editIdentity: Identity NOW is SUSPENDED ", aimBase.Id)
		} else if funcEdit == REVOKESTATUS {
			if aimStatus.Status == REVOKESTATUS {
				Log(WARNING, "tenIdentities editIdentity: Identity is already REVOKED", aimBase.Id)
				return shim.Error(ValResp(409, "editIdentity error: conflict with the current status (REVOKED) of the resource."))
			}
			aimStatus.Status = REVOKESTATUS
			Log(DEBUG, "tenIdentities editIdentity: Identity NOW is REVOKED ", aimBase.Id)
		} else {
			Log(ERROR, "tenIdentities editIdentity function Edit not compatible: ", strconv.Itoa(funcEdit))
			return shim.Error(ValResp(400, "Invalid invoke function edit name !(ACTIVE, SUSPEND, REVOKE)"))
		}

		// Create ID KEY
		IDKey, err = stub.CreateCompositeKey(AIMSTATUS, []string{aimStatus.Id, aimStatus.Start})
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity CreateCompositeKey IdentityStatus err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		// Convert Object AimStatus in []Byte
		byteAimStatus, err = json.Marshal(&aimStatus)
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity jsonMarshal IdentityStatus err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		// PUT STATE INTO THE LEDGER
		err = stub.PutState(IDKey, byteAimStatus)
		if err != nil {
			Log(ERROR, "tenIdentities editIdentity putState(IdentityStatus) err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		Log(INFO, "tenIdentities editIdentity putState(IdentityStatus) is OK with address: ", IDKey)
	}

	aim.IdentityBase = aimBase
	aim.IdentityStatus = aimStatus
	byteAim, err = json.Marshal(aim)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity Marshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAim)
	Log(INFO, "tenIdentities editIdentity editStatus is OK with response:", jsonResp)
	return shim.Success([]byte(ValResponse(204, "tenIdentities editIdentity editStatus is OK", string(byteAim))))

}

//getIdentity: the input json must contain the KEY of the object Aim to SEARCH, [Aim = aimBase + aimStatus]

func (t *tenIdentities) getIdentity(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	Log(INFO, "tenIdentities getIdentity: ", time.Now().Format(time.RFC3339))
	Log(INFO, "tenIdentities ON getIdentity with Address: ", args[0])

	var jsonResp string
	var err error
	var aim Identity = Identity{}
	var byteAim []byte
	var datetime time.Time
	var idCa = ""
	var desc string
	var addrCaller string
	var addrToGet string
	var signature string
	var text string

	// Control args[]
	if len(args) != 3 && len(args) != 4 {
		Log(ERROR, "tenIdentities getIdentity numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getIdentity ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}
	addrCaller = args[0]
	addrToGet = args[1]

	if len(args) == 3 {
		signature = args[2]
		datetime = time.Now()
		text = GETIDENTITY + addrCaller + addrToGet
	} else if len(args) == 4 { // 3 arguments (Address, Address and Date) -> return the version valid at the Date
		signature = args[3]
		datetime, err = time.Parse(time.RFC3339, args[2])
		text = GETIDENTITY + addrCaller + addrToGet + args[2]
		if err != nil {
			Log(ERROR, "tenIdentities getIdentity time.Parse args[1] Data Format is not correct", args[2])
			return shim.Error(ValResp(400, err.Error()))
		}
	} else {
		Log(ERROR, "tenIdentities getIdentity numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getIdentity ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities editIdentity getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	text = text + idCa
	/*
		   		identity = getCaller
		   		validate
		   		if addrCaller ^= aimBase.Id
		   			identity = getIdentity
			   	return identity
	*/

	aim, desc, err = SearchIdentity(stub, addrCaller, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentity Addr Caller err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aim == Identity{}) {
		Log(ERROR, "tenIdentities getIdentity Addr Caller NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities getIdentity Addr Caller NOTFOUND "+desc))
	}

	// Validate the Signature of the Caller
	validate, desc, err := ValidateChallengeQueue(text, aim.PKey, aim.KType, signature)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentity ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if !validate {
		Log(ERROR, desc, "")
		return shim.Error(ValResp(401, desc))
	} else {
		Log(INFO, "tenIdentities getIdentity validate signature ", desc)
	}

	// The caller is not the identity to get
	if addrCaller != addrToGet {
		// aim = getIdentity (id)

		aim, desc, err = SearchIdentity(stub, addrToGet, datetime)
		if err != nil {
			Log(ERROR, "tenIdentities getIdentity Addr ToGet err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		if (aim == Identity{}) {
			Log(ERROR, "tenIdentities getIdentity Addr ToGet NOTFOUND ", desc)
			return shim.Error(ValResp(404, "tenIdentities getIdentity Addr ToGet NOTFOUND "+desc))
		}
	}

	byteAim, err = json.Marshal(aim)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentity Marshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAim)
	Log(INFO, "tenIdentities getIdentity getState is OK with response:", jsonResp)
	return shim.Success([]byte(ValResponse(200, "tenIdentities getIdentity getState is OK", string(byteAim))))

}

//getAllIdentities: getALL
//The caller must be a ANY USER!!!

func (t *tenIdentities) getAllIdentities(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	Log(INFO, "tenIdentities getAllIdentities: ", time.Now().Format(time.RFC3339))
	// Log(INFO, "tenIdentities ON getAllIdentities with Address: ", args[0])

	var jsonResp string
	var err error
	var aimBase IdentityBase = IdentityBase{}
	var aimBaseEntries []IdentityBase
	var aims []Identity
	var aim Identity = Identity{}

	var aimStatus IdentityStatus = IdentityStatus{}
	var byteAims []byte
	var datetime time.Time
	var checkControl bool
	var desc string
	var text string
	var idCa string = ""

	// Control args[]
	if len(args) != 2 {
		Log(ERROR, "tenIdentities getAllIdentities numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getAllIdentities ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	addr := args[0]
	sign := args[1]

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities getAllIdentities getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	text = GETALLIDENTITIES + idCa
	datetime = time.Now()

	aim, desc, err = SearchIdentity(stub, addr, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities getAllIdentities Addr err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aim == Identity{}) {
		Log(ERROR, "tenIdentities getAllIdentities Addr NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities getAllIdentities Addr NOTFOUND "+desc))
	}

	// Validate the Signature of the Caller
	validate, desc, err := ValidateChallengeQueue(text, aim.PKey, aim.KType, sign)
	if err != nil {
		Log(ERROR, "tenIdentities getAllIdentities ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if !validate {
		Log(ERROR, desc, "")
		return shim.Error(ValResp(401, desc))
	} else {
		Log(INFO, "tenIdentities getAllIdentities validate signature ", desc)
	}

	// Search aimBase
	aimBaseEntries, err = GetStateByPartialCompositeKeyBaseEntry(stub)
	if err != nil {
		Log(ERROR, "tenIdentities GetAllIdentities GetStateByPartialCompositeKeyIdentityBase err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if len(aimBaseEntries) == 0 || aimBaseEntries == nil {
		Log(ERROR, "tenIdentities GetAllIdentities GetStateByPartialCompositeKeyIdentityBase DATA NOT FOUND", "")
		return shim.Error(ValResp(404, "tenIdentities getAllIdentities getAllIdentitiesBase DATA NOT FOUND"))
	}

	for i := 0; i < len(aimBaseEntries); i++ {
		aimBase = aimBaseEntries[i]
		datetime = time.Now()

		aimStatus, err = GetIdentityStatus(stub, aimBase.Id, datetime)
		if err != nil {
			Log(ERROR, "tenIdentities getIdentity getAllIdentitiesStatus err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		if (aimStatus == IdentityStatus{}) {
			Log(ERROR, "tenIdentities getAllIdentities getAllIdentitiesStatus DATA NOT FOUND", args[0])
			return shim.Error(ValResp(404, "tenIdentities getAllIdentities GetIdentity Status DATA NOT FOUND"))
		}
		if aimStatus.Status == ACTIVESTATUS {
			checkControl = true
			if aimBase.End != aimBase.Id {
				checkControl, desc, err = ControlChainOfTrust(stub, aimBase.End, datetime)

				if err != nil {
					Log(ERROR, "tenIdentities getAllIdentities ControlChainOfTrust err not nil", err.Error())
					return shim.Error(ValResp(400, err.Error()))
				}
				if !checkControl {
					Log(ERROR, "getAllIdentities error: invalid Identity  ", desc)
				}
			}
			if checkControl {
				aim.IdentityBase = aimBase
				aim.IdentityStatus = aimStatus
				aims = append(aims, aim)
			}
		}
	}

	byteAims, err = json.Marshal(aims)
	if err != nil {
		Log(ERROR, "tenIdentities getAllIdentities Marshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAims)
	Log(INFO, "tenIdentities getAllIdentities getState is OK with response:", jsonResp)
	return shim.Success([]byte(ValResponse(200, "tenIdentities getAllIdentities getState is OK", string(byteAims))))

}

///////////////////////////////////////////////////////////////////////////////////
//getIdentitiesFiltered: get Identities by parameter of EXT field
//The caller must be a ANY USER!!!

func (t *tenIdentities) getIdentitiesFiltered(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	Log(INFO, "tenIdentities getIdentitiesFiltered: ", time.Now().Format(time.RFC3339))

	var jsonResp string
	var err error
	var aimBase IdentityBase = IdentityBase{}
	var aimBaseEntries []IdentityBase
	var aims []Identity
	var aim Identity = Identity{}

	var aimStatus IdentityStatus = IdentityStatus{}
	var byteAims []byte
	var datetime time.Time
	var checkControl bool
	var desc string
	var idCa string

	// Control args[]
	if len(args) != 4 {
		Log(ERROR, "tenIdentities getIdentitiesFiltered numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getIdentitiesFiltered ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	Log(INFO, "tenIdentities ON getIdentitiesFiltered addr       : ", args[0])
	Log(INFO, "tenIdentities ON getIdentitiesFiltered by selector: ", args[1])
	Log(INFO, "tenIdentities ON getIdentitiesFiltered by value   : ", args[2])
	Log(INFO, "tenIdentities ON getIdentitiesFiltered signature  : ", args[3])

	addr := args[0]
	selector := args[1]
	value := args[2]
	signature := args[3]

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	text := GETIDENTITIESFILTERED + selector + value + idCa
	datetime = time.Now()
	aim, desc, err = SearchIdentity(stub, addr, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered Addr err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aim == Identity{}) {
		Log(ERROR, "tenIdentities getIdentitiesFiltered Addr NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities getIdentitiesFiltered Addr NOTFOUND "+desc))
	}

	// Validate the Signature of the Caller
	validate, desc, err := ValidateChallengeQueue(text, aim.PKey, aim.KType, signature)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if !validate {
		Log(ERROR, desc, "")
		return shim.Error(ValResp(401, desc))
	} else {
		Log(INFO, "tenIdentities getIdentitiesFiltered validate signature ", desc)
	}

	// Search aimBase
	aimBaseEntries, err = GetStateByPartialCompositeKeyBaseEntry(stub)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered GetStateByPartialCompositeKeyIdentityBase err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if len(aimBaseEntries) == 0 || aimBaseEntries == nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered GetStateByPartialCompositeKeyIdentityBase DATA NOT FOUND", "")
		return shim.Error(ValResp(404, "tenIdentities getIdentitiesFiltered getAllIdentitiesBase DATA NOT FOUND"))
	}

	for i := 0; i < len(aimBaseEntries); i++ {
		aimBase = aimBaseEntries[i]
		datetime = time.Now()

		aimStatus, err = GetIdentityStatus(stub, aimBase.Id, datetime)
		if err != nil {
			Log(ERROR, "tenIdentities getIdentity getAllIdentitiesStatus err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		if (aimStatus == IdentityStatus{}) {
			Log(ERROR, "tenIdentities getIdentitiesFiltered getAllIdentitiesStatus DATA NOT FOUND", args[0])
			return shim.Error(ValResp(404, "tenIdentities getIdentitiesFiltered GetIdentity Status DATA NOT FOUND"))
		}
		if aimStatus.Status == ACTIVESTATUS {
			checkControl = true
			if aimBase.End != aimBase.Id {
				checkControl, desc, err = ControlChainOfTrust(stub, aimBase.End, datetime)

				if err != nil {
					Log(ERROR, "tenIdentities getIdentitiesFiltered ControlChainOfTrust err not nil", err.Error())
					return shim.Error(ValResp(400, err.Error()))
				}
				if !checkControl {
					Log(ERROR, "getIdentitiesFiltered error: invalid Identity  ", desc)
				}
			}
			if checkControl {
				// Check selector value of ext
				// selector = "$."+selector
				v := interface{}(nil)
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered EXT field  : ", aimBase.Ext)
				st, err := strconv.Unquote(aimBase.Ext)
				if err != nil {
					Log(ERROR, "tenIdentities getIdentitiesFiltered Unquote err not nil", err.Error())
					return shim.Error(ValResp(400, err.Error()))
				}

				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered EXT unquote: ", st)
				json.Unmarshal([]byte(st), &v)

				// json.Unmarshal([]byte(aimBase.Ext),&v)
				if err != nil {
					Log(ERROR, "tenIdentities getIdentitiesFiltered Unmarshal err not nil", err.Error())
					return shim.Error(ValResp(400, err.Error()))
				}
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered EXT field  : ", aimBase.Ext)
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered interface  : ", v)
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered by selector: ", selector)
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered by value   : ", value)
				welcome, err := jsonpath.Get(selector, v)
				if err != nil {
					Log(ERROR, "tenIdentities getIdentitiesFiltered JSONpath err not nil", err.Error())
					return shim.Error(ValResp(400, err.Error()))
				}
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered welcome    : ", welcome)
				var str string
				str = fmt.Sprintf("%v", welcome)
				Log(DEBUG, "tenIdentities ON getIdentitiesFiltered by string  : ", str)

				if strings.Contains(str, value) {
					Log(DEBUG, "tenIdentities ON getIdentitiesFiltered VALUE FOUND: ", value)
					aim.IdentityBase = aimBase
					aim.IdentityStatus = aimStatus
					aims = append(aims, aim)
				}
			}
		}
	}

	byteAims, err = json.Marshal(aims)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered Marshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAims)
	Log(INFO, "tenIdentities getIdentitiesFiltered getState is OK with response:", jsonResp)
	return shim.Success([]byte(ValResponse(200, "tenIdentities getIdentitiesFiltered getState is OK", string(byteAims))))

}

////////////////////////////////////////////////////////////////////////////////////
//getHierarchyIdentity: the input json must contain the KEY of the object identity to search for the hierarchy

func (t *tenIdentities) getHierarchyIdentity(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	Log(INFO, "tenIdentities getHierarchyIdentity: ", time.Now().Format(time.RFC3339))
	Log(INFO, "tenIdentities ON getHierarchyIdentity with Address: ", args[0])

	var jsonResp string
	var err error
	var aimBase IdentityBase = IdentityBase{}
	var aimStatus IdentityStatus = IdentityStatus{}
	var aim Identity = Identity{}
	var byteAims []byte
	var datetime time.Time
	//var found bool
	//var role string
	var checkControl bool
	var desc string
	var idCa string
	var text string
	var signature string
	var identities []Identity

	// Control args[]
	if len(args) != 3 && len(args) != 4 {
		Log(ERROR, "tenIdentities getHierarchyIdentity numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getHierarchyIdentity ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	addrCaller := args[0]
	addrToGet := args[1]
	if len(args) == 3 {
		signature = args[2]
		datetime = time.Now()
		text = GETHIERARCHYIDENTITY + addrCaller + addrToGet
	} else if len(args) == 4 { // 3 arguments (Address, Address and Date) -> return the version valid at the Date
		signature = args[3]
		datetime, err = time.Parse(time.RFC3339, args[2])
		text = GETHIERARCHYIDENTITY + addrCaller + addrToGet + args[2]
		if err != nil {
			Log(ERROR, "tenIdentities getHierarchyIdentity time.Parse args[1] Data Format is not correct", args[2])
			return shim.Error(ValResp(400, err.Error()))
		}
	} else {
		Log(ERROR, "tenIdentities getHierarchyIdentity numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities getHierarchyIdentity ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities getIdentitiesFiltered getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	// Control Aim into CA and check the authorization to getIdentity
	// GetAttributeValue(ROLE) - notfound is KO
	/*
		found, role, err = isInvokerOperator(stub, ROLE)
		if err != nil {
			Log(ERROR, "getHierarchyIdentity error: isInvokerOperator(role) err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}
		if !found {
			Log(ERROR, "getHierarchyIdentity error: ROLE is empty!!!", "")
			return shim.Error(ValResp(404, "tenIdentities getHierarchyIdentity ERROR: ROLE is empty!!!"))
		}
		Log(DEBUG, "getHierarchyIdentity error: isInvokerOperator by role: ", role)
		if role != ADMIN && role != USER {
			Log(ERROR, "getHierarchyIdentity error: ROLE is not authorized to get an Identity!!", role)
			return shim.Error(ValResp(401, "tenIdentities getHierarchyIdentity ERROR: ROLE is not authorized to get an Identity!!"))
		}

	*/
	text = text + idCa
	aim, desc, err = SearchIdentity(stub, addrCaller, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities getHierarchyIdentity Addr Caller err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aim == Identity{}) {
		Log(ERROR, "tenIdentities getHierarchyIdentity Addr Caller NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities getHierarchyIdentity Addr Caller NOTFOUND "+desc))
	}

	// Validate the Signature of the Caller
	validate, desc, err := ValidateChallengeQueue(text, aim.PKey, aim.KType, signature)
	if err != nil {
		Log(ERROR, "tenIdentities getHierarchyIdentity ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if !validate {
		Log(ERROR, desc, "")
		return shim.Error(ValResp(401, desc))
	} else {
		Log(INFO, "tenIdentities getHierarchyIdentity validate signature ", desc)
	}

	aimBase, err = GetIdentityBase(stub, addrToGet)
	if err != nil {
		Log(ERROR, "tenIdentities getHierarchyIdentity GetIdentityBase err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aimBase == IdentityBase{}) {
		Log(ERROR, "tenIdentities getHierarchyIdentity GetIdentityBase DATA NOT FOUND", args[0])
		return shim.Error(ValResp(404, "tenIdentities getHierarchyIdentity GetIdentityBase DATA NOT FOUND"))
	}

	aim.IdentityBase = aimBase

	if len(args) == 1 {
		datetime = time.Now()
	} else { // 2 arguments (Address and Date) -> return the version valid at the Date
		datetime, err = time.Parse(time.RFC3339, args[1])
		if err != nil {
			Log(ERROR, "tenIdentities getHierarchyIdentity time.Parse args[1] Data Format is not correct", args[1])
			return shim.Error(ValResp(400, err.Error()))
		}
	}

	aimStatus, err = GetIdentityStatus(stub, aimBase.Id, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities getHierarchyIdentity GetIdentityStatus err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	if (aimStatus == IdentityStatus{}) {
		Log(ERROR, "tenIdentities getHierarchyIdentity GetIdentityStatus DATA NOT FOUND", args[0])
		return shim.Error(ValResp(404, "tenIdentities getHierarchyIdentity GetIdentity Status DATA NOT FOUND"))
	}
	aim.IdentityStatus = aimStatus

	if aimStatus.Status != ACTIVESTATUS {
		Log(ERROR, "tenIdentities getHierarchyIdentity STATUS of Identity is not ACTIVE: ", strconv.Itoa(aimStatus.Status))
		return shim.Error(ValResp(400, "getHierarchyIdentity error: invalid Identity (invalid status)"+strconv.Itoa(aimStatus.Status)))
	}
	if aim.IdentityBase.End != aim.IdentityBase.Id {
		identities, checkControl, desc, err = ExtractChainOfTrust(stub, aim, datetime)

		if err != nil {
			Log(ERROR, "tenIdentities getHierarchyIdentity ControlChainOfTrust err not nil", err.Error())
			return shim.Error(ValResp(400, err.Error()))
		}

		if !checkControl {
			Log(ERROR, "getHierarchyIdentity error: invalid identity  ", desc)
			return shim.Error(ValResp(400, "editIdentity error: invalid identity : "+desc))
		}
	} else {
		Log(INFO, "tenIdentities getHierarchyIdentity getState is OK and the IDENTITY is a ROOT", aim.IdentityBase.Id)
	}

	byteAims, err = json.Marshal(identities)
	if err != nil {
		Log(ERROR, "tenIdentities getHierarchyIdentity Marshal err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	jsonResp = string(byteAims)
	Log(INFO, "tenIdentities getHierarchyIdentity getState is OK with response:", jsonResp)
	return shim.Success([]byte(ValResponse(200, "tenIdentities getHierarchyIdentity getState is OK", string(byteAims))))

	// return shim.Success(byteAim)
}

//verifySignature: verify the signature of text in input

func (t *tenIdentities) verifySignature(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	Log(INFO, "tenIdentities verifySignature: ", time.Now().Format(time.RFC3339))

	var err error
	var aim Identity = Identity{}
	var datetime time.Time
	var desc string
	var validate bool
	var text string
	var addr string
	var sign string
	var idCa = ""

	// Control args[]
	if len(args) != 3 {
		Log(ERROR, "tenIdentities verifySignature numbers of Args not correctly!: ", strconv.Itoa(len(args)))
		return shim.Error(ValResp(400, "tenIdentities verifySignature ERROR: numbers of Args not correctly: "+strconv.Itoa(len(args))))
	}

	addr = args[0]
	sign = args[2]
	datetime = time.Now()

	idCa, err = getIdSubjectCert(stub)
	if err != nil {
		Log(ERROR, "tenIdentities verifySignature getIdSubjectCert err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	text = args[1] + idCa
	// LEDGER
	aim, desc, err = SearchIdentity(stub, addr, datetime)
	if err != nil {
		Log(ERROR, "tenIdentities verifySignature Addr err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}
	if (aim == Identity{}) {
		Log(ERROR, "tenIdentities verifySignature Addr NOTFOUND ", desc)
		return shim.Error(ValResp(400, "tenIdentities verifySignature Addr NOTFOUND "+desc))
	}

	validate, desc, err = ValidateChallengeQueue(text, aim.PKey, aim.KType, sign)
	if err != nil {
		Log(ERROR, "tenIdentities verifySignature ValidateChallengeQueue err not nil", err.Error())
		return shim.Error(ValResp(400, err.Error()))
	}

	Log(INFO, "tenIdentities verifySignature validate signature ", desc)
	return shim.Success([]byte(strconv.FormatBool(validate)))

}

////////////////////////////////////////////////////////////////////////////////////
// UTILITY

// SearchIdentity getIdentityBase + getIdentityStatus with relatives controls
func SearchIdentity(stub shim.ChaincodeStubInterface, id string, datetime time.Time) (Identity, string, error) {

	var err error
	var aimBase IdentityBase = IdentityBase{}
	var aimStatus IdentityStatus = IdentityStatus{}
	var aim Identity = Identity{}
	var checkControl bool
	var desc string
	var control string
	// LEDGER
	aimBase, err = GetIdentityBase(stub, id)
	if err != nil {
		desc = "tenIdentities SearchIdentity GetIdentityBase err not nil"
		Log(ERROR, desc, err.Error())
		return aim, desc, err
	}
	if (aimBase == IdentityBase{}) {
		desc = "tenIdentities SearchIdentity GetIdentityBase DATA NOT FOUND "
		Log(ERROR, desc, id)
		return aim, desc, nil
	}

	aim.IdentityBase = aimBase
	// LEDGER
	aimStatus, err = GetIdentityStatus(stub, aimBase.Id, datetime)
	if err != nil {
		desc = "tenIdentities SearchIdentity GetIdentityStatus err not nil"
		Log(ERROR, desc, err.Error())
		return Identity{}, desc, err
	}

	if (aimStatus == IdentityStatus{}) {
		desc = "tenIdentities SearchIdentity GetIdentityStatus DATA NOT FOUND"
		Log(ERROR, desc, id)
		return Identity{}, desc, nil
	}
	aim.IdentityStatus = aimStatus

	if aimStatus.Status != ACTIVESTATUS {
		desc = "tenIdentities SearchIdentity STATUS of Identity is not ACTIVE: " + strconv.Itoa(aimStatus.Status)
		Log(ERROR, desc, id)
		return Identity{}, desc, nil
	}
	// LEDGER
	if aim.IdentityBase.End != aim.IdentityBase.Id {
		checkControl, control, err = ControlChainOfTrust(stub, aim.IdentityBase.End, datetime)

		if err != nil {
			desc = "tenIdentities SearchIdentity ControlChainOfTrust err not nil - "
			Log(ERROR, desc+control, err.Error())
			return Identity{}, desc + control, err
		}

		if !checkControl {
			desc = "SearchIdentity error: invalid Identity "
			Log(ERROR, desc, control)
			return Identity{}, desc + control, nil
		}
	} else {
		desc = "tenIdentities SearchIdentity getState is OK and the Identity is a ROOT"
		Log(INFO, desc, aim.IdentityBase.Id)
		return aim, desc, nil
	}
	desc = "tenIdentities SearchIdentity getState is OK"
	return aim, desc, nil
}

// GetIdentityBase Gate AimBase of a aim by id
func GetIdentityBase(stub shim.ChaincodeStubInterface, id string) (IdentityBase, error) {
	var ab IdentityBase = IdentityBase{}
	var err error
	var IDKey string
	var byteAb []byte

	// GetAimBase with this Id
	// Create the KEY
	IDKey, err = stub.CreateCompositeKey(AIMBASE, []string{id})
	if err != nil {
		Log(ERROR, "tenIdentities GetIdentityBase CreateCompositeKey err not nil", err.Error())
		return ab, err
	}

	Log(DEBUG, "tenIdentities GetIdentityBase for the key:", IDKey)
	byteAb, err = stub.GetState(IDKey)
	if err != nil {
		Log(ERROR, "tenIdentities GetIdentityBase GetState err not nil", err.Error())
		return ab, err
	}
	if byteAb == nil {
		Log(WARNING, "tenIdentities GetIdentityBase response is nil", "")
		return ab, nil
	}
	err = json.Unmarshal(byteAb, &ab)
	if err != nil {
		Log(ERROR, "tenIdentities GetIdentityBase Unmarshal err not nil", err.Error())
		return ab, err
	}
	Log(INFO, "tenIdentities GetIdentityBase response is OK", "")
	return ab, nil
}

// GetIdentityStatus Get AimStatus of a AIM by id and Time
func GetIdentityStatus(stub shim.ChaincodeStubInterface, id string, datetime time.Time) (IdentityStatus, error) {
	var as IdentityStatus = IdentityStatus{}
	var err error
	var AimStatusEntries []IdentityStatus
	var validFrom time.Time

	// Search aimStatus
	AimStatusEntries, err = GetStateByPartialCompositeKeyStatusEntry(stub, []string{id})
	if err != nil {
		Log(ERROR, "tenIdentities GetIdentityStatus GetStateByPartialCompositeKeyIdentityStatus err not nil", err.Error())
		return as, err
	}
	if len(AimStatusEntries) == 0 || AimStatusEntries == nil {
		Log(ERROR, "tenIdentities GetIdentityStatus GetStateByPartialCompositeKeyIdentityStatus DATA NOT FOUND", id)
		return as, nil
	}

	// loop for search the version valid a the date
	validFrom, err = time.Parse(time.RFC3339, AimStatusEntries[0].Start)

	Log(DEBUG, "tenIdentities GetIdentityStatus COMPARE Start[0] Data Format is: ", validFrom.String())
	Log(DEBUG, "tenIdentities GetIdentityStatus COMPARE datetime par Data Format is: ", datetime.String())

	if err != nil {
		Log(ERROR, "tenIdentities GetIdentityStatus time.Parse ValidFrom[0] Data Format is not correct", AimStatusEntries[0].Start)
		return as, err
	}

	if validFrom.After(datetime) { // datetime is lower all ValidFrom of aimStatus (first date validity)
		as = IdentityStatus{}
		Log(WARNING, "tenIdentities GetIdentityStatus First IdentityStatus is greater of datatime parameter: ", datetime.String())
	} else {
		as = AimStatusEntries[0]

		for i := 1; i < len(AimStatusEntries); i++ {

			validFrom, err = time.Parse(time.RFC3339, AimStatusEntries[i].Start)
			if err != nil {
				Log(ERROR, "tenIdentities GetIdentityStatus ParseAny ValidFrom[i] Data Format is not correct", AimStatusEntries[i].Start)
				return as, err
			}

			if validFrom.After(datetime) {
				as = AimStatusEntries[i-1]
				break
			} else {
				as = AimStatusEntries[i]
			}
		}
	}
	// TEST NOT FOUND
	if as == (IdentityStatus{}) {
		Log(WARNING, "tenIdentities GetIdentityStatus Loop for data validity - DATA NOT FOUND for date: ", datetime.String())
	}
	return as, nil
}

// GetStateByPartialCompositeKeyBaseEntry return a array of aimStatus by key
func GetStateByPartialCompositeKeyBaseEntry(stub shim.ChaincodeStubInterface) ([]IdentityBase, error) {

	var buffer bytes.Buffer
	var err error

	var aimBase IdentityBase
	var aimBaseEntries []IdentityBase

	resultsIterator, err := stub.GetStateByPartialCompositeKey(AIMBASE, []string(nil))
	if err != nil {
		Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityBase err not nil", err.Error())
		return nil, err
	}

	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults

	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	var i int

	for i = 0; resultsIterator.HasNext(); i++ {
		responseRange, err := resultsIterator.Next()
		if err != nil {
			Log(ERROR, "tenIdentities Into Loop  resultsIterator.Next() for err not nil", strconv.Itoa(i))
			return nil, err
		}

		Log(DEBUG, "tenIdentities Into Loop pre stub.SplitCompositeKey", responseRange.Key)
		_, compositeKeyParts, err := stub.SplitCompositeKey(responseRange.Key)
		if err != nil {
			Log(ERROR, "tenIdentities Into Loop  stub.SplitCompositeKey for err not nil", responseRange.Key)
			return nil, err
		}

		keyComplete, err := stub.CreateCompositeKey(AIMBASE, compositeKeyParts)

		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityBase CreateCompositeKey err not nil", err.Error())
			return nil, err
		}

		Log(DEBUG, "tenIdentities Into Loop post stub.SplitCompositeKey", keyComplete)

		// idAsBytes, err := stub.GetState(idReturn)
		idAsBytes, err := stub.GetState(keyComplete)
		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityBase GetState: ", err.Error())
			return nil, err
		}
		if idAsBytes == nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityBase GetState DATA NOT FOUND for KEY: ", keyComplete)
		}

		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(keyComplete)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(idAsBytes))

		err = json.Unmarshal(idAsBytes, &aimBase)
		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyMessage Unmarshal IdentityStatus", err.Error())
			return nil, err
		}
		aimBaseEntries = append(aimBaseEntries, aimBase)

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return aimBaseEntries, nil

}

// GetStateByPartialCompositeKeyStatusEntry return a array of aimStatus by key
func GetStateByPartialCompositeKeyStatusEntry(stub shim.ChaincodeStubInterface, KEYid []string) ([]IdentityStatus, error) {

	var buffer bytes.Buffer
	var err error

	var aimStatus IdentityStatus
	var aimStatusEntries []IdentityStatus

	resultsIterator, err := stub.GetStateByPartialCompositeKey(AIMSTATUS, KEYid)
	if err != nil {
		Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityStatus err not nil", err.Error())
		return nil, err
	}

	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults

	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	var i int

	for i = 0; resultsIterator.HasNext(); i++ {
		responseRange, err := resultsIterator.Next()
		if err != nil {
			Log(ERROR, "tenIdentities Into Loop resultsIterator.Next() for err not nil", strconv.Itoa(i))
			return nil, err
		}

		Log(DEBUG, "tenIdentities Into Loop pre stub.SplitCompositeKey", responseRange.Key)
		_, compositeKeyParts, err := stub.SplitCompositeKey(responseRange.Key)
		if err != nil {
			Log(ERROR, "tenIdentities Into Loop  stub.SplitCompositeKey for err not nil", responseRange.Key)
			return nil, err
		}

		keyComplete, err := stub.CreateCompositeKey(AIMSTATUS, compositeKeyParts)

		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityStatus CreateCompositeKey err not nil", err.Error())
			return nil, err
		}

		Log(DEBUG, "tenIdentities Into Loop post stub.SplitCompositeKey", keyComplete)

		// idAsBytes, err := stub.GetState(idReturn)
		idAsBytes, err := stub.GetState(keyComplete)
		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityStatus GetState: ", err.Error())
			return nil, err
		}
		if idAsBytes == nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyIdentityStatus GetState DATA NOT FOUND for KEY: ", keyComplete)
		}

		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(keyComplete)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(idAsBytes))

		err = json.Unmarshal(idAsBytes, &aimStatus)
		if err != nil {
			Log(ERROR, "tenIdentities GetStateByPartialCompositeKeyMessage Unmarshal IdentityStatus", err.Error())
			return nil, err
		}
		aimStatusEntries = append(aimStatusEntries, aimStatus)

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	sort.Slice(aimStatusEntries, func(i, j int) bool {
		return aimStatusEntries[i].Start < aimStatusEntries[j].Start
	})

	return aimStatusEntries, nil

}

// ControlChainOfTrust System of control for the Endorser of Aim
func ControlChainOfTrust(stub shim.ChaincodeStubInterface, accessAddress string, time time.Time) (bool, string, error) {
	var desc string = ""
	var ab IdentityBase = IdentityBase{}
	var as IdentityStatus = IdentityStatus{}
	// var accessAddress string = ""
	var err error
	var i int

	i = 0
	// The first time i'm sure that Address is not Endorser (v. Call)
	ab.Id = ""
	ab.End = accessAddress
	// The first time i'm sure that Status is Active (v. Call)
	as.Status = ACTIVESTATUS

	for ab.End != ab.Id && as.Status == ACTIVESTATUS && i < NUMMAXGEN {
		i++
		accessAddress = ab.End
		ab, err = GetIdentityBase(stub, accessAddress)
		if err != nil {
			ab, err = GetIdentityBase(stub, accessAddress)
			Log(ERROR, "tenIdentities ControlChainOfTrust GetIdentityBase err not nil", err.Error())
			desc = err.Error()
			break
		}
		as, err = GetIdentityStatus(stub, accessAddress, time)
		if err != nil {
			Log(ERROR, "(chain of trust would exceed length limit)", err.Error())
			desc = err.Error()
			break
		}
	}
	if err != nil {
		return false, desc, err
	}
	if i >= NUMMAXGEN {
		Log(ERROR, "(chain of trust would exceed length limit): ", as.Id)
		desc = "(chain of trust would exceed length limit): " + as.Id
		return false, desc, nil
	}
	if as.Status != ACTIVESTATUS {
		Log(ERROR, "(Endorser is not active or chain of trust is broken): ", as.Id+" - "+(strconv.Itoa(as.Status)))
		desc = "(Endorser is not active or chain of trust is broken): " + as.Id + " - " + (strconv.Itoa(as.Status))
		return false, desc, nil
	}
	if ab.End == ab.Id {
		Log(INFO, "tenIdentities ControlChainOfTrust RETURN IS OK for: ", as.Id)
		desc = "tenIdentities ControlChainOfTrust Found identity Endorser Root: " + as.Id
		return true, desc, nil
	}
	// Never
	desc = "tenIdentities ControlChainOfTrust There is some problem with condition exit of loop for " + as.Id
	return false, desc, nil
}

// ExtractChainOfTrust System of control for the Endorser of Aim
func ExtractChainOfTrust(stub shim.ChaincodeStubInterface, accessIdentity Identity, time time.Time) ([]Identity, bool, string, error) {
	var desc string = ""
	var ab IdentityBase = IdentityBase{}
	var as IdentityStatus = IdentityStatus{}
	var accessAddress string = ""
	var err error
	var i int
	var identity = Identity{}
	var identities []Identity

	i = 0
	// The first time i'm sure that Address is not Endorser (v. Call)
	ab = accessIdentity.IdentityBase
	as = accessIdentity.IdentityStatus

	identities = append(identities, accessIdentity)

	for ab.End != ab.Id && as.Status == ACTIVESTATUS && i < NUMMAXGEN {
		i++
		accessAddress = ab.End
		ab, err = GetIdentityBase(stub, accessAddress)
		if err != nil {
			ab, err = GetIdentityBase(stub, accessAddress)
			Log(ERROR, "tenIdentities ExtractChainOfTrust GetIdentityBase err not nil", err.Error())
			desc = err.Error()
			break
		}
		as, err = GetIdentityStatus(stub, accessAddress, time)
		if err != nil {
			Log(ERROR, "(ExtractChainOfTrust - chain of trust would exceed length limit)", err.Error())
			desc = err.Error()
			break
		}
		identity.IdentityBase = ab
		identity.IdentityStatus = as
		identities = append(identities, identity)
	}

	if err != nil {
		return identities, false, desc, err
	}
	if i >= NUMMAXGEN {
		Log(ERROR, "(ExtractChainOfTrust - chain of trust would exceed length limit): ", as.Id)
		desc = "(chain of trust would exceed length limit): " + as.Id
		return identities, false, desc, nil
	}
	if as.Status != ACTIVESTATUS {
		Log(ERROR, "ExtractChainOfTrust - (Endorser is not active or chain of trust is broken): ", as.Id+" - "+(strconv.Itoa(as.Status)))
		desc = "(Endorser is not active or chain of trust is broken): " + as.Id + " - " + (strconv.Itoa(as.Status))
		return identities, false, desc, nil
	}
	if ab.End == ab.Id {
		Log(INFO, "tenIdentities ExtractChainOfTrust RETURN IS OK for: ", as.Id)
		desc = "tenIdentities ExtractChainOfTrust Found identity Endorser Root: " + as.Id
		return identities, true, desc, nil
	}
	// Never
	desc = "tenIdentities ExtractChainOfTrust There is some problem with condition exit of loop for " + as.Id
	return nil, false, desc, nil
}

/////////////////////////////////////////////////////////////////////////////

// Get the randomText from challenge queue with id of endorser
// get randomText from library ////////////////////////////////////////////////////////////

// ValidateChallengeQueue System of validate the signature of random text connected with Endorser of Aim
func ValidateChallengeQueue(text string, PbKey string, KType string, sign string) (bool, string, error) {
	var desc string = ""
	var err error
	var validate bool = false
	var epkxy ECDSApublicKeyXY = ECDSApublicKeyXY{}
	var rpken RSApublicKeyEN = RSApublicKeyEN{}
	var ecdsaPublicKey *ecdsa.PublicKey
	var rsaPublicKey *rsa.PublicKey

	randomText := text
	Log(DEBUG, "tenIdentities ValidateChallengeQueue Signature (IN)    : ", sign)
	Log(DEBUG, "tenIdentities ValidateChallengeQueue Text              : ", text)

	// Switch case for the algorithm of public key (only ECDSA and RSA are admitted at the moment)
	if KType == ECDSA {

		var signature Signature = Signature{}
		err = json.Unmarshal([]byte(sign), &signature)
		if err != nil {
			Log(ERROR, "tenIdentities ValidateChallengeQueue jsonUnmarshal signature err not nil", err.Error())
			desc = "tenIdentities ValidateChallengeQueue jsonUnmarshal signature err not nil"
			return validate, desc, err
		}

		Log(DEBUG, "tenIdentities ValidateChallengeQueue Identity.PKeyBlob : ", PbKey)
		Log(DEBUG, "tenIdentities ValidateChallengeQueue signature.R  : ", signature.R)
		Log(DEBUG, "tenIdentities ValidateChallengeQueue signature.S  : ", signature.S)

		// create the public key ecdsa of the Endorser for the control of the signature
		// Unmarshal in a structure complex with curve details
		err = json.Unmarshal([]byte(PbKey), &epkxy)
		if err != nil {
			Log(ERROR, "tenIdentities ValidateChallengeQueue jsonUnmarshal publicKey ECDSA err not nil", err.Error())
			desc = "tenIdentities ValidateChallengeQueue jsonUnmarshal publicKey ECDSA err not nil"
			return validate, desc, err
		}

		ecdsaPublicKey = hexToPublicKey(epkxy)
		if err != nil {
			Log(ERROR, "tenIdentities ValidateChallengeQueue hexToPublicKey publicKey ECDSA err not nil", err.Error())
			desc = "tenIdentities ValidateChallengeQueue hexToPublicKey publicKey ECDSA err not nil"
			return validate, desc, err
		}
		// validateEcdsaSignature validate a pkeyblob (regenerate) signed with the public key relative at the signature
		validate = validateEcdsaSignature(ecdsaPublicKey, randomText, signature)
		if !validate {
			Log(ERROR, "ValidateChallengeQueue error: Endorser signature of Challenge Queue failed", randomText)
			desc = "ValidateChallengeQueue error: Endorser signature of Challenge Queue failed"
		} else {
			desc = "tenIdentities ValidateChallengeQueue Validate of Signature of Challenge Queue is OK"
			Log(DEBUG, "tenIdentities ValidateChallengeQueue Validate of Signature of Challenge Queue: TRUE!!!!!!!", "")
		}

		return validate, desc, nil

	} else if KType == RSA {
		// create the public key rsa of the End for the control of the signature
		// Unmarshal in a structure complex with details
		err = json.Unmarshal([]byte(PbKey), &rpken)
		if err != nil {
			Log(ERROR, "tenIdentities ValidateChallengeQueue jsonUnmarshal publicKey RSA err not nil", err.Error())
			desc = "tenIdentities ValidateChallengeQueue jsonUnmarshal publicKey RSA err not nil"
			return validate, desc, err
		}
		rsaPublicKey, err = hexToRSAPublicKey(rpken)
		if err != nil {
			Log(ERROR, "tenIdentities ValidateChallengeQueue hexToRSAPublicKey publicKey RSA err conv string to int E", err.Error())
			desc = "tenIdentities ValidateChallengeQueue hexToRSAPublicKey publicKey RSA err conv string to int E"
			return validate, desc, err
		}

		// validateEcdsaSignature validate a pkeyblob (regenerate) signed with the public key relative at the signature
		validate, err = validateRsaSignature(rsaPublicKey, randomText, sign)
		if !validate || err != nil {
			Log(ERROR, "ValidateChallengeQueue error: Endorser signature of Challenge Queue failed", randomText)
			desc = "ValidateChallengeQueue error: Endorser signature of Challenge Queue failed"
		} else {
			Log(DEBUG, "tenIdentities ValidateChallengeQueue Validate of Signature of Challenge Queue: TRUE!!!!!!!", "")
			desc = "tenIdentities ValidateChallengeQueue Validate of Signature of Challenge Queue is OK"
		}
		return validate, desc, nil

	} else {
		Log(ERROR, "tenIdentities ValidateChallengeQueue ERROR: algorithm for the Public Key is not admitted!! ", KType)
		desc = "tenIdentities ValidateChallengeQueue ERROR: algorithm for the Public Key is not admitted!! " + KType
		return validate, desc, nil
	}
}

/////////////////////////////////////////////////////////////////////////////

// createAddressFromPKeyBlob creates a Addess from pKeyBlob input parameters
func createAddressFromPKeyBlob(pKey string) string {
	byteAddrCalc32 := sha256.Sum256([]byte(pKey))
	return base58.Encode([]byte(byteAddrCalc32[:]))
}

// main function is only relevant in unit test mode. Only included here for completeness.
func main() {
	LogLevel = DEBUG
	caAccess = true
	// Create a new Smart Contract
	err := shim.Start(new(tenIdentities))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
