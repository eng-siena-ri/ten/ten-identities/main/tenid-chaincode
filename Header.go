package main

// Header
type Header struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}
