package main

// Identity : Account Identity Manifest
type Identity struct {
	IdentityBase   `json:"identityBase"`
	IdentityStatus `json:"identityStatus"`
}
